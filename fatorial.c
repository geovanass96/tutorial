#include "fatorial.h"

int fatorial(int x){
	if(x > 0){
          int i, fat = 1;
          for(i = 1; i <= x; i++)
              fat = fat * i;
           return fat;
        }
	return -1;  //retorna -1 caso o numero seja negativo ou seja o zero
}
